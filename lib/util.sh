# Modified from Judith Hoefer's ftag-helper code.


cptoweb() 
{
# copy from lxplus
target_dir=/afs/cern.ch/atlas/groups/validation/Btagging/$1/$2/
[ -d $target_dir ] || mkdir -p $target_dir
echo "Copying plots/ ROC/ and ROC_VRTrack to $target_dir"
cp -r plots/ ROC* $target_dir
[ $? ] && echo "Copying Done. plots uploaded to http://atlas-computing.web.cern.ch/atlas-computing/links/PhysValDir/Btagging/$1/$2/$target_dir"
[ $? ] || echo "Oops. Copying Failed. "

}

scptoweb()
{
# copy from different server to lxplus
echo "using scptoweb"
target_dir=/afs/cern.ch/atlas/groups/validation/Btagging/$1/$2/
echo $USERNAME
echo ssh $USERNAME@lxplus.cern.ch mkdir -p $target_dir
ssh $USERNAME@lxplus.cern.ch mkdir -p $target_dir
echo "Copying plots/ ROC/ and ROC_VRTrack to $target_dir"
scp -r plots/ ROC* $USERNAME@lxplus.cern.ch:$target_dir
[ $? ] && echo "ssh Copying Done. plots uploaded to http://atlas-computing.web.cern.ch/atlas-computing/links/PhysValDir/Btagging/$1/$2/"
[ $? ] || echo "Oops. Copying Failed. "
}

bulk_scptoweb()
{
# copy from different server to lxplus in bulk
echo "using scptoweb"
target_dir=/afs/cern.ch/atlas/groups/validation/Btagging/$1/
echo $USERNAME
echo ssh $USERNAME@lxplus.cern.ch mkdir -p $target_dir
#ssh $USERNAME@lxplus.cern.ch mkdir -p $target_dir
echo "Copying plots/ ROC/ and ROC_VRTrack to $target_dir"	
echo ls ./$1/
for d  in `ls ./$1/`
do 
	echo $d
	echo ssh $USERNAME@lxplus.cern.ch mkdir -p $target_dir/$d
	echo scp -r $1/$d/plots $1/$d/ROC*  $USERNAME@lxplus.cern.ch:$target_dir/$d

	read
	ssh $USERNAME@lxplus.cern.ch mkdir -p $target_dir/$d
	scp -r $1/$d/plots $1/$d/ROC* $USERNAME@lxplus.cern.ch:$target_dir/$d
done 

#echo scp -R $1/*/plots/ $1/*/ROC/ $1/*/ROC_VRTrack/ $USERNAME@lxplus.cern.ch:$target_dir
#scp -r $1/*/plots/ $1/*/ROC/ $1/*/ROC_VRTrack/ $USERNAME@lxplus.cern.ch:$target_dir
[ $? ] && echo "ssh Copying Done. plots uploaded to http://atlas-computing.web.cern.ch/atlas-computing/links/PhysValDir/Btagging/$1/$2/"
[ $? ] || echo "Oops. Copying Failed. "
}

ruciosetup()
{
    lsetup rucio
    if [[ voms-proxy-info ]]
	then 
         echo "proxy certificate already exist"
    else
         voms-proxy-init -voms atlas
    fi
}

ruciols()
{
    #arg1: ttbar/Zprime #arg2: recostring
    echo $1
    echo $DSID
    if [ $1=="EZprime" ]
        then  $DSID=800030; echo $1 
    elif [ $1=="Zprime" ]
        then  $DSID=801271; echo $1
    elif [ $1=="ttbar" ]
        then $DSID=601229
    fi
    echo DSID
    reco_tag=$2
    container=$3
    rucio  ls --filter type=DATASET $3:$3*$DSID*NTUP*$reco_tag*tid*
    if [ $(rucio  ls --filter type=DATASET $3:$3*$DSID*NTUP*$reco_tag*tid* | wc -l) == 5 ]
    then
        echo "ruciols(): One unique dataset found."
	return true
    else
        echo "ruciols(): Uh oh, None or more than one unique dataset found for Sample: $1, DSID: $DSID, tag: $reco_tag "
	return false
    fi
}

ruciodownload(){ #arg1: ttbar/Zprime #arg2: recostring #arg3: folder
    lsetup rucio
    echo $1
    if [ $1=="Zprime" ]
        then $DSID=801271
    elif [ $1=="ttbar" ]
        then $DSID=601229
    elif [ $1=="EZprime" ]
        then $DSID=800030
    fi
    echo DSID $DSID
    reco_tag=$2
    container_tag=$3
    folder=$4

    root_dir=${PWD}
    [ -d $4 ] || mkdir $4
    cd $4
    ruciols $1 $2 $3
	if [ $? ]
	then 
		echo "One unique dataset found. Proceed with downloading files."
		rucio download --filter type=DATASET $container_tag:$container_tag*$DSID*NTUP*$reco_tag*tid* 
	else
			read -p "Proceed with downloading the file(s) to directory $4/ ?(y/n)" yn
			case $yn in
				[yY] ) echo "Downloading with Rucio...";
						rucio download --filter type=DATASET $container_tag:$container_tag*$DSID*NTUP*$reco_tag*tid* ;;
				[nN] ) echo "Stopping and not downloading the files";;
				*    ) echo "Invalid response."
					esac
	fi
    cd $root_dir
}

cprunscript(){
    # arg1: directory
    # arg2: sample type: ZPrime or ttbar
    sample=$1
    if [[ -z RUNSCRIPTDIR  ]]
    then
        RUNSCRIPTDIR="../athena_run_scripts/"
    fi
    echo "Getting run scripts from directory $RUNSCRIPTDIR ..."
    cp -v $RUNSCRIPTDIR/joint/mergePhysValFiles.py $RUNSCRIPTDIR/joint/add_line_breaks_to_html.sh $RUNSCRIPTDIR/joint/CreatePhysValWebPage.py $RUNSCRIPTDIR/joint/Draw_PhysVal_btagROC.c .
}

makesubdirs(){
  mkdir files_merged plots $@
}

cpscripts() {
  cp ../../mergePhysValFiles.py ../../add_line_breaks_to_html.sh ../../CreatePhysValWebPage.py ../../Draw_PhysVal_btagROC.c .
  #../../Draw_PhysVal_btagROC.c ../../Draw_PhysVal_btagROC_VRTrack.c 
}

cpscripts_pre_low_level_vars() {
  cp ../../mergePhysValFiles_pre_low_level_vars.py mergePhysValFiles.py
  cp ../../Draw_PhysVal_btagROC_pre_low_level_vars.c Draw_PhysVal_btagROC.c
}

applymerging() {
    # arg1: file1
    # arg2: file2
  echo "  now merging the files in those folders: "
  ls $1
  ls $2
  echo "" >> log.txt
  echo "merging reference" >> log.txt
  echo "python3 mergePhysValFiles.py -i ./$1/* -o files_merged/merged_NTUP_PHYSVAL_ref.root -d BTag >> log.txt"
  python3 mergePhysValFiles.py -i ./$1/* -o files_merged/merged_NTUP_PHYSVAL_ref.root -d BTag >> log.txt
  echo "" >> log.txt
  echo "merging test" >> log.txt
  echo "python3 mergePhysValFiles.py -i ./$2/* -o files_merged/merged_NTUP_PHYSVAL_test.root -d BTag >> log.txt"
  python3 mergePhysValFiles.py -i ./$2/* -o files_merged/merged_NTUP_PHYSVAL_test.root -d BTag >> log.txt
  echo "  ls files_merged"
  ls files_merged
}

makewebdisplay() {
  #setupATLAS --quiet
  # asetup 21.0.20,here
  #asetup Athena,22.0.32
  echo "" >> log.txt
  echo "make webdisplay" >> log.txt
  physval_make_web_display.py --reffile $1:files_merged/merged_NTUP_PHYSVAL_ref.root --outdir=plots files_merged/merged_NTUP_PHYSVAL_test.root --title=$2 --logy --normalize --ratio --startpath="BTag" >> log.txt
  bash add_line_breaks_to_html.sh
}

makeROCcurves() {
  lsetup "root 6.30.02-x86_64-el9-gcc13-opt"
  echo "" >> log.txt

  # Script needs to be updated to EZprime range
  if [ $1=="EZprime" ] 
  then
     $1="Zprime"
  fi

  echo "making dir: mkdir -p ROC_$2/eff_vs_pt_$1 and mkdir -p ROC_$2/eff_vs_Lxy"
  mkdir -p ROC_$2/eff_vs_pt_$1
  mkdir -p ROC_$2/eff_vs_Lxy
  echo "make ROC curves" >> log.txt
  echo sed -i "71s/.*/TString jetType=\"$2\";/" Draw_PhysVal_btagROC.c; 
  echo 
  sed -i "71s/.*/TString jetType=\"$2\";/" Draw_PhysVal_btagROC.c; 
  echo 
  echo root -l -b -q "Draw_PhysVal_btagROC.c+(\"$1\", \"$2\")" 
  root -l -b -q "Draw_PhysVal_btagROC.c+(\"$1\", \"$2\")" >> ROC_log.txt
  python3 CreatePhysValWebPage.py -i ROC_$2/
}

