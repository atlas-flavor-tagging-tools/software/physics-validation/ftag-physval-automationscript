#!/bin/bash

#Author: Yvonne Ng; yvonne.ng@desy.de 2023-4-1

Run_setupAthena(){
echo "---Setting up Athena---"
echo $1
setupATLAS
echo "asetup Athena,$1"
asetup Athena,$1
}

Run_setupRucio(){
echo "---Setting up rucio---"
echo "Running: \"ruciosetup\""
ruciosetup
}

Run_makesubdir(){
echo "---Making subfolder in the task directory---"
echo "Running: \"makesubdirs $file1 $file2\""
sleep 5
makesubdirs $file1 $file2
}

Run_cprunscript(){
echo "---Coping runscripts to the task directory---"
echo "Running: \"cprunscript $sample\""
echo cprunscript $sample
sleep 5
cprunscript $sample
}

Run_ruciodownload1(){
echo "---Downloading $file1 sample from Rucio---"
echo "Running: \"ruciodownload $sample $reco_tag1 $scope_tag1 $file1\""
ruciodownload $sample $reco_tag1 $scope_tag1 $file1
}

Run_ruciodownload2(){
echo "---Downloading $file2 sample from Rucio---"
echo "Running: \"ruciodownload $sample $reco_tag2 $scope_tag2 $file2\""
ruciodownload $sample $reco_tag2 $scope_tag2 $file2
}

Run_mergefiles(){
echo "---Merging files downloaded from Rucio ---"
lsetup "root 6.30.02-x86_64-el9-gcc13-opt"

echo "Running: \"applymerging $file1 $file2\" "
sleep 5
applymerging $file1 $file2
}

Run_mergefilessolo(){
echo "---Merging files downloaded from Rucio ---"
echo "Running: \"applymerging $file1\" "
sleep 5
applymerging $file1
}


Run_makeplots(){
echo "---Make plots and webdisplay ---"
echo "Running: \"makewebdisplay $file1 $file2\" "
sleep 5
makewebdisplay $file1 $file2
#phyvalmake_web_display.py  --outdir=plots $MERGED_TESTFILE --title=$TEST_NAME --logy --normalize --ratio --startpath="BTag" --ratiorange=0.5
}

Run_makeplotssolo(){
echo "---Make plots and webdisplay ---"
echo "Running: \"makewebdisplay $file1 $file2\" "
sleep 5 
makewebdisplaysolo $file1
}

Run_makeROC(){
echo "---Making ROCcurves---"
echo "Running\"makeROCcurves $sample JETTYPE; makeROCcurves_VRTrack $sample JETTYPE\" "
sleep 5
makeROCcurves $sample PFlow
makeROCcurves $sample VR
}

Run_cptoweb(){
echo "---Copying plots to atlas web---"
echo "Running: \"cptoweb ${run_date} ${task_name}_${sample}\" "
sleep 5
HOSTNAME=`hostname`
echo HOSTNAME: $HOSTNAME
if [[ HOSTNAME == *lxplus* ]]
then 
cptoweb ${run_date} ${task_name}_${sample}
else 
scptoweb ${run_date} ${task_name}_${sample}
fi 
}

Run_bulk_cptoweb(){
echo "---Copying plots to atlas web---"
echo "Running: \"cptoweb ${run_date} ${task_name}_${sample}\" "
sleep 5
HOSTNAME=`hostname`
echo HOSTNAME: $HOSTNAME
bulk_scptoweb ${run_date} ${task_name}_${sample}
}

