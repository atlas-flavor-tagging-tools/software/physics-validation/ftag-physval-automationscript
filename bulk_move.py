import os
import argparse
#Author: Yvonne Ng; yvonne.ng@desy.de; 2023-4-1

parser=argparse.ArgumentParser(description="")
parser.add_argument("--folder", type=str, required=True)
args=parser.parse_args()

subdirs=os.listdir(args.folder)
folder=args.folder
username="ywng"

target_dir="/afs/cern.ch/atlas/groups/validation/Btagging/"

fileslist_to_scp=[]
dict_file_list=dict([(subdir,[]) for subdir in subdirs ])

for subdir in subdirs:
	dict_file_list[subdir].append("%s/%s/plots"%(folder, subdir))
	dict_file_list[subdir].append("%s/%s/ROC*"%(folder, subdir))
	
file_list=" ".join(fileslist_to_scp)
create_dirs=[target_dir+"/"+folder+"/"+subdir for subdir in subdirs]
create_dir_list=" ".join(create_dirs)

dict_files=dict([(key," ".join(dict_file_list[key])) for key in dict_file_list])


local_dir_list=" ".join(subdirs)
print("dict_files", dict_files)
print("remote_dir_list: ", create_dir_list)

new_plot_dir=folder+"_plots"

for key, value in dict_files.items():
	command="mkdir -p %s/%s"%(new_plot_dir, key)
	new_plot_subdir=new_plot_dir+"/"+key
	command2="cp -r %s %s"%(value, new_plot_subdir)
	print("command1: ", command)
	print("command2: ", command2)
	os.system(command)
	os.system(command2)
	

command3="ssh %s@lxplus.cern.ch mkdir -p %s/%s"%(username, target_dir, folder)
print("command3: ", command3)
os.system(command3)

command4="scp -r %s/* ywng@lxplus.cern.ch:%s/%s"%(new_plot_dir, target_dir, folder)
print("command4: ", command4)
os.system(command4)
