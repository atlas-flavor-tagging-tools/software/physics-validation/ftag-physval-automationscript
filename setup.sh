#!/bin/bash

ROOTDIR=$PWD
RUNSCRIPTDIR=$PWD/athena_run_scripts
source ~/.bashrc
setupATLAS
source lib/util.sh
source lib/runlib.sh
ruciosetup

# For scp/rucio
# --Change it to your username
USERNAME=$USER


